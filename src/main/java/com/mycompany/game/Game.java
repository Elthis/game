/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.game;

import com.elthis.objects.Camera;
import com.elthis.objects.GameLevel;
import com.elthis.objects.GameObject;
import com.elthis.objects.Hero;
import com.elthis.objects.component.Collider2D;
import com.elthis.objects.component.Component;
import com.elthis.repository.Repository;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import javafx.beans.Observable;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.value.ObservableValue;
import static javafx.print.PrintColor.COLOR;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.control.Alert;

/**
 *
 * @author Elthis
 */
public class Game {
    GraphicsContext graphics;
    ArrayList<GameObject> objectList = new ArrayList();
    ArrayList<Component> components = new ArrayList();
    ArrayList<GameObject> addList = new ArrayList();
    ArrayList<GameObject> removeList = new ArrayList();
    public ArrayList getComponents()
    {
        return components;
    }
    public GraphicsContext getGraphicsContext()
    {
        return graphics;
    }
    public void instantiate(GameObject go)
    {
        go.init();
        go.setParent(this);
        addList.add(go);
    }
    public void destroy(GameObject go)
    {
        removeList.add(go);
    }
    private void setUpRepository()
    {
        try
        {
            Repository.add("heroImage",new Image(new FileInputStream("nin.png") ));
            Repository.add("monsterImage",new Image(new FileInputStream("monster.png") ));
            Repository.add("spellImage",new Image(new FileInputStream("spell.png")));
            Repository.add("dirtImage",new Image(new FileInputStream("dirt.png")));
            Repository.add("wallImage",new Image(new FileInputStream("wall.png")));
            Repository.add("fireBallSpriteSheet", new Image(new FileInputStream("fireBall.png")));
            Repository.add("explosionSpriteSheet", new Image(new FileInputStream("explosion.png")));
            Repository.add("backgroundImage", new Image(new FileInputStream("bg.png")));
        }
        catch(FileNotFoundException fnot)
        {
            new Alert(Alert.AlertType.ERROR,fnot.getMessage()+" not found");
        }
    }
    Camera camera;
    public Camera getMainCamera()
    {
        return camera;
    }
    public Game(GraphicsContext gc)
    {
        graphics = gc;
        graphics.setFill(Color.BLACK);
        setUpRepository();
        camera = new Camera();
        objectList.add(new GameLevel());
        objectList.add(new Hero());
        objectList.add(camera);
        objectList.forEach((go) -> {
            go.setParent(this);
            go.init();
        });
    }
    public void loop()
    {
        update();
        render();
    }
    public void update()
    {
        objectList.forEach((go) -> {
            components.addAll(go.getComponents());
        });
        components.forEach((component)->{
            component.prepare();
        });
        while(components.size()>0)
        {
            Component component = components.get(0);
            component.update();
            components.remove(component);
        }
        objectList.forEach((go) -> {
            go.update();
        });
        objectList.removeAll(removeList);
        removeList.clear();
        objectList.addAll(addList);
        addList.clear();
    }
    public void render()
    {
        double canvasWidth = graphics.getCanvas().getWidth();
        double canvasHeight = graphics.getCanvas().getHeight();  
        graphics.fillRect(0, 0, canvasWidth, canvasHeight);
        objectList.forEach((go) -> {
            
            if(go.getPos().getX()+canvasWidth>=camera.getPos().getX() && go.getPos().getX()<=camera.getPos().getX()+canvasWidth)
            {           
                go.render(graphics,camera.getPos());
            }
            
        });
       
    }
    
}
