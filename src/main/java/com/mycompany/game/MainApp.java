package com.mycompany.game;

import com.elthis.input.Input;
import com.elthis.time.Time;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;


public class MainApp extends Application {

    public static void main(String[] args) 
    {
        launch(args);
    }
 
    @Override
    public void start(Stage theStage) 
    {
    theStage.setTitle( "Giera" );
    Group root = new Group();
    Scene theScene = new Scene( root );
    theScene.setOnKeyPressed(
            new EventHandler<KeyEvent>()
            {
                @Override
                public void handle(KeyEvent e)
                {
                    String code = e.getCode().toString();
                    Input.keyDown(code);
                }
            });
 
    theScene.setOnKeyReleased(
            new EventHandler<KeyEvent>()
            {
                @Override
                public void handle(KeyEvent e)
                {
                    String code = e.getCode().toString();
                    Input.keyUp(code);
                }
            });
 
    theScene.addEventFilter(MouseEvent.DRAG_DETECTED , new EventHandler<MouseEvent>() {
    @Override
    public void handle(MouseEvent mouseEvent) {
        theScene.startFullDrag();
    }
});
    theScene.setOnMousePressed(
        new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent e)
            {
                Input.setMousePos(e.getX(), e.getY());
                MouseButton button = e.getButton();
                if(button==MouseButton.PRIMARY)
                    Input.keyDown("LMB");
                if(button==MouseButton.SECONDARY)
                    Input.keyDown("RMB");
                if(button==MouseButton.MIDDLE)
                    Input.keyDown("MMB");
            }
        });
    theScene.setOnMouseReleased(
        new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent e)
            {
                Input.setMousePos(e.getX(), e.getY());
                MouseButton button = e.getButton();
                if(button==MouseButton.PRIMARY)
                    Input.keyUp("LMB");
                if(button==MouseButton.SECONDARY)
                    Input.keyUp("RMB");
                if(button==MouseButton.MIDDLE)
                    Input.keyUp("MMB");
            }
        });
    theScene.setOnMouseMoved(
        new EventHandler<MouseEvent>()
        {
            @Override
            public void handle(MouseEvent e)
            {
                Input.setMousePos(e.getX(), e.getY());
            }
        }
    );
    theScene.setOnMouseDragOver(new EventHandler<MouseEvent>() {
    @Override
    public void handle(MouseEvent e) {
        Input.setMousePos(e.getX(), e.getY());
    } 
});
    Canvas canvas = new Canvas( 1280, 720 );
    root.getChildren().add( canvas );
 
    final GraphicsContext gc = canvas.getGraphicsContext2D();
 
    theStage.setScene(theScene);
    Time.reset();
    new AnimationTimer()
    {
        Game game = new Game(gc);
        
        @Override
        public void handle(long currentNanoTime)
        {
            Time.reset();
            game.loop();
 
        }
    }.start();
         
    theStage.show();
    }

}
