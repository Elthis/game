/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.time;

/**
 *
 * @author Elthis
 */
public abstract class Time {
    private static long startNanoTime = System.nanoTime();
    private static double delta = 0.0;
    static public double getDelta()
    {
        return delta;
    }
    
    static public void reset()
    {
        long now = System.nanoTime();
        delta = (now - startNanoTime)/10000000.0;
        startNanoTime = now;
    }
}
