/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.input;

import java.util.ArrayList;
import javafx.geometry.Point2D;


/**
 *
 * @author Elthis
 */
public abstract class Input {
    private static ArrayList<String> input = new ArrayList<>();
    private static double x;
    private static double y;
    static public void keyDown(String code)
    {
        if(!input.contains(code))
            input.add(code);
    }
    
    static public void keyUp(String code)
    {
        input.remove(code);
    }
    
    static public boolean isPressed(String code)
    {
        return input.contains(code);
    }
    
    static public void setMousePos(double nx, double ny)
    {
        x = nx;
        y = ny;
    }
    
    static public Point2D getMousePos()
    {
        return new Point2D(x,y);
    }
  
}
