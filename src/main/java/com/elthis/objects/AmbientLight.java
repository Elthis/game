/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.objects;

import com.mycompany.game.Game;
import java.util.ArrayList;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

/**
 *
 * @author Elthis
 */
public class AmbientLight extends GameObject{
    Image lightMask;
    Canvas canv;
    GraphicsContext gc;
    ArrayList<PointLight> pointLightList = new ArrayList();
    @Override
    public void init()
    {
        canv = new Canvas(((Game)getParent()).getGraphicsContext().getCanvas().getWidth(), ((Game)getParent()).getGraphicsContext().getCanvas().getHeight());
        gc = canv.getGraphicsContext2D();
        super.init();
    }
    public void addPointLight(PointLight pl)
    {
        pointLightList.add(pl);
    }
    @Override
    public void update() {
        gc.setStroke(new Color(0.5,0,0,0.5));
        gc.setFill(new Color(0.2,0.2,0.2,1.0));
        gc.fillRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getHeight()); 
        pointLightList.forEach((pl) -> {
            
            gc.setFill(pl.getColor());
            gc.fillOval(pl.getPos().getX()-pl.getRadius(), pl.getPos().getY()-pl.getRadius(), pl.getRadius()*2, pl.getRadius()*2);
        });
        lightMask = createImage(gc.getCanvas());
        pointLightList.clear();
        super.update();
    }
    
    public Image createImage(Node node) {

        WritableImage wi;

        SnapshotParameters parameters = new SnapshotParameters();
        parameters.setFill(Color.TRANSPARENT);

        int imageWidth = (int) node.getBoundsInLocal().getWidth();
        int imageHeight = (int) node.getBoundsInLocal().getHeight();

        wi = new WritableImage(imageWidth, imageHeight);
        node.snapshot(parameters, wi);

        return wi;

    }

    @Override
    public void render(GraphicsContext gc, Point2D camera) {
        gc.save();
        gc.setGlobalBlendMode(BlendMode.MULTIPLY);
        gc.drawImage(lightMask, 0, 0);
        gc.restore();
        super.render(gc,camera);
    }
}
