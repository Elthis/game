/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.objects;

import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.Paint;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;

/**
 *
 * @author Elthis
 */
public class PointLight extends GameObject {
    protected double radius;
    protected double intensity;
    protected Color color;
    
    public PointLight(Color col, double rad,double intens)
    {
        color = col;
        radius = rad;
        intensity = intens;
    }
    
    public double getRadius()
    {
        return radius;
    }
    public Paint getColor()
    {
        RadialGradient gradient1 = new RadialGradient(0, 0.0, 0.5, 0.5, 0.5, true, CycleMethod.NO_CYCLE, new Stop[] {
        new Stop(0,  color),
        new Stop(intensity, Color.TRANSPARENT)
    });
        return gradient1;
    }
}
