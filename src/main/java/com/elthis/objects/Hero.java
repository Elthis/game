/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.objects;

import com.elthis.graphics.AnimatedSprite;
import com.elthis.input.Input;
import com.elthis.objects.component.Collider2D;
import com.elthis.time.Time;
import com.mycompany.game.Game;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author Elthis
 */
public class Hero extends GameObject {

    private final Double speed = 2.0;
    private double cooldown = 0.0;

    @Override
    public void init() {
        addChild(new AnimatedSprite("heroImage", 64, 64));
        addComponent(new Collider2D(60,60));
        setPos(640, 360);
        super.init();
    }

    @Override
    public void update() {
        Camera cam = ((Game) getParent()).getMainCamera();
        PointLight pl = new PointLight(new Color(1.0,1.0,1.0,1.0), 160,1.0);
        pl.setPos(posX-cam.posX, posY-cam.posY);
        cam.al.addPointLight(pl);
        if (cooldown > 0.0) {
            cooldown -= Time.getDelta();
        } else if (cooldown < 0.0) {
            cooldown = 0.0;
        }

        if (Input.isPressed("D")) {
            posX += speed * Time.getDelta();
            cam.posX += speed * Time.getDelta();
        }

        if (Input.isPressed("A")) {
            posX -= speed * Time.getDelta();
            cam.posX -= speed * Time.getDelta();
        }
        if (Input.isPressed("W")) {
            posY -= speed * Time.getDelta();
            cam.posY -= speed * Time.getDelta();
        }
        if (Input.isPressed("S")) {
            posY += speed * Time.getDelta();
            cam.posY += speed * Time.getDelta();
        }
        if (Input.isPressed("LMB") && cooldown <= 0.0) {
            Point2D mouse = Input.getMousePos();
            Point2D dir = mouse.subtract(new Point2D(posX-cam.posX, posY-cam.posY)).normalize();
            Spell n = new Spell();
            n.init();
            n.setPos(posX, posY);
            n.setDirection(dir);
            addChild(n);
            cooldown += 16.0;
        }
        super.update();

    }

    @Override
    public void render(GraphicsContext gc, Point2D camera) {
        super.render(gc, camera);
    }

}
