/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.objects;

import com.elthis.graphics.AnimatedSprite;
import com.elthis.input.Input;
import com.elthis.objects.component.Collider2D;
import com.elthis.time.Time;
import com.mycompany.game.Game;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author Elthis
 */
public class Monster extends GameObject {
    @Override
    public void init() {
        addChild(new AnimatedSprite("monsterImage", 64, 64));
        addComponent(new Collider2D(60,60));
        setPos(320, 320);
        super.init();
    }

    @Override
    public void update() {
        
        super.update();

    }

    @Override
    public void render(GraphicsContext gc, Point2D camera) {
        super.render(gc, camera);
    }
}
