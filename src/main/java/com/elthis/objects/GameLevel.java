/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.objects;


import com.elthis.objects.environment.BrickWall;
import com.elthis.objects.environment.Dirt;
import com.mycompany.game.Game;
import java.util.Random;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author Elthis
 */
public class GameLevel extends GameObject {
    

    @Override
    public void init() {
        for(int x = 0; x < 1280; x+=128)
            for(int y=0; y < 1280; y+=128)
            {
                GameObject d;    
                if(x == 0 || x == 1280-128 || y == 0 || y == 1280-128)
                    d=new BrickWall();
                else 
                    d = new Dirt(); 
                d.setPos(x, y);
                this.addChild(d);
            }
        this.addChild(new Monster());
        super.init();
    }

    @Override
    public void update() {
        super.update();
    }

    @Override
    public void render(GraphicsContext gc, Point2D camera) {
        super.render(gc,camera);
    }
    
}
