/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.objects.environment;

import com.elthis.objects.GameObject;
import com.elthis.objects.component.Collider2D;
import com.elthis.repository.Repository;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 *
 * @author Elthis
 */
public class BrickWall extends GameObject {

    Image bitmap;
    @Override
    public void init() {
        bitmap = Repository.findByKey("wallImage");
        addComponent(new Collider2D(128,128));
        
    }

    @Override
    public void update() {
        
    }

    @Override
    public void render(GraphicsContext gc, Point2D camera) {
       gc.drawImage(bitmap, posX-camera.getX()-64, posY-camera.getY()-64);
       super.render(gc, camera);
    }
}
