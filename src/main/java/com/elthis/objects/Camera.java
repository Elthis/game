/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.objects;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author Elthis
 */
public class Camera extends GameObject{
    AmbientLight al = new AmbientLight();
    public AmbientLight getAmbientLight()
    {
        return al;
    }
    @Override
    public void init()
    {
        al.setParent(parent);
        al.init();
        super.init();
    }

    @Override
    public void update() {
        al.update();
        super.update();
    }

    @Override
    public void render(GraphicsContext gc, Point2D camera) {
        al.render(gc, camera);
        super.render(gc,camera);
    }
}
