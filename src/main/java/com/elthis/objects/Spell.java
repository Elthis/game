/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.objects;

import com.elthis.graphics.AnimatedSprite;
import com.elthis.input.Input;
import com.elthis.objects.component.Collider2D;
import com.elthis.objects.lights.SpellLight;
import com.elthis.repository.Repository;
import com.elthis.time.Time;
import com.mycompany.game.Game;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 *
 * @author Elthis
 */
public class Spell extends GameObject{
    Image bitmap;
    private final double speed = 4.0;
    Point2D direction;
    PointLight pl,p2;
    @Override
    public void init()
    {
        addComponent(new Collider2D(30,30));
        addChild(new AnimatedSprite("fireBallSpriteSheet",64,64));
        pl = new PointLight(new Color(1.0,0.2,0.2,1.0), 80,0.9);
        p2 = new PointLight(new Color(1.0,1.0,0.5,1.0), 24,1.0);
        super.init();
    }
    public void setDirection(Point2D dir)
    {
        direction = dir;
        
        rotation = dir.angle(new Point2D(1.0,0.0));
        if(dir.getY()<0.0)
            rotation = -rotation;
    }
    @Override
    public void update() {
        super.update();
        ArrayList<GameObject> colisions= this.<Collider2D>getComponent().getCollisions();
        if(!colisions.isEmpty())
        {
            for(GameObject go : colisions)
                if(!(go instanceof Spell || go == this.getParent()))
                {
                    GameObject exp = new Explosion();
                    exp.setPos(this.getPos());
                    this.getGame().instantiate(exp);
                    this.<GameObject>getParent().destroyObject(this);
                    return;
                }
            
        }
        posX+= direction.getX()*Time.getDelta()*speed;
        posY+= direction.getY()*Time.getDelta()*speed;
        Camera cam = ((Game) ((GameObject)getParent()).getParent()).getMainCamera();
        pl.setPos(posX-cam.posX, posY-cam.posY);
        p2.setPos(posX-cam.posX, posY-cam.posY);
        //cam.al.addPointLight(pl);
         cam.al.addPointLight(p2);
    }

    @Override
    public void render(GraphicsContext gc, Point2D camera) {
        
        super.render(gc,camera);
    }
}
