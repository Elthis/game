/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.objects.lights;

import com.elthis.objects.PointLight;
import com.elthis.time.Time;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.Paint;
import javafx.scene.paint.RadialGradient;
import javafx.scene.paint.Stop;

/**
 *
 * @author Elthis
 */
public class SpellLight extends PointLight {
    double time = 0;
    public SpellLight(Color col, double rad,double intens)
    {
        super(col,rad,intens);
    }
    @Override
    public Paint getColor()
    {
        time+=Time.getDelta()/50.0;
        RadialGradient gradient1 = new RadialGradient(0, 0.0, 0.5, 0.5, 0.5, true, CycleMethod.NO_CYCLE, new Stop[] {
        new Stop(0,  color),
        new Stop(intensity/2 + intensity/2*Math.abs(Math.cos(time)), Color.TRANSPARENT)
    });
        return gradient1;
    }
    @Override
    public double getRadius()
    {
        return radius/2+radius/2*Math.abs(Math.cos(time));
    }
}
