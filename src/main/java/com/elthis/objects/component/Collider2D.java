/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.objects.component;

import com.elthis.objects.GameObject;
import com.mycompany.game.Game;
import java.util.ArrayList;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

/**
 *
 * @author Elthis
 */
public class Collider2D extends GameObject implements Component {
   double width, height;
   ArrayList<GameObject> collidedWith;
   public Collider2D(double w, double h)
   {
       width = w;
       height = h;
       collidedWith = new ArrayList();
   }
   public ArrayList<GameObject> getCollisions()
   {
       return collidedWith;
   }
   @Override
   public void update()
   {
        Game game = this.getGame();
        ArrayList<Component> components = game.getComponents();
        for(Component compo : components )
        {
            if(compo instanceof Collider2D)
            {
                if(compo != this)
                 checkCollision((Collider2D) compo);
            }
        }
        super.update();
        
   }

   public void checkCollision(Collider2D other)
   {
       if(intersect(other))
       {
          collidedWith.add(other.getParent());
          other.getCollisions().add(this.getParent());
       }
   }
   private boolean intersect(Collider2D other)
   {
       return (this.posX < other.posX+other.width ) &&
               (this.posX+this.width > other.posX) &&
               (this.posY < other.posY +other.height) &&
               (this.posY+this.height > other.posY);
   }
   @Override
   public void render(GraphicsContext gc, Point2D camera)
   {
       //gc.save();
       //gc.setStroke(Color.BLUE);
      // gc.setLineWidth(5);
      // gc.strokeRect(posX-camera.getX(), posY-camera.getY(), width, height);
      // gc.restore();
   }
    @Override
    public void prepare() {
        collidedWith.clear();
        this.setPos(this.<GameObject>getParent().getPos().getX()-width/2, this.<GameObject>getParent().getPos().getY()-height/2);
    }
}
