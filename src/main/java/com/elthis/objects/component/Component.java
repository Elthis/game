/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.objects.component;

import com.elthis.objects.Camera;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author Elthis
 */
public interface Component {
    
    public void prepare();
    public void update();
    public void setParent(Object obj);
    public void render(GraphicsContext gc, Point2D cam);
}
