/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.objects;

import com.elthis.graphics.AnimatedSprite;
import com.elthis.objects.component.Collider2D;
import com.elthis.time.Time;
import com.mycompany.game.Game;
import javafx.scene.paint.Color;

/**
 *
 * @author Elthis
 */
public class Explosion extends GameObject{
    PointLight p2;
    double lifeTime;
    @Override
    public void init()
    {
        addChild(new AnimatedSprite("explosionSpriteSheet",64,64));
        p2 = new PointLight(new Color(1.0,1.0,0.5,1.0), 24,1.0);
        lifeTime = 0;
        super.init();
    }
    
    @Override
    public void update()
    {
        lifeTime+=Time.getDelta();
        if(lifeTime > 96)
        {
            this.getGame().destroy(this);
            return;
        }
        p2 = new PointLight(new Color(1.0,1.0,0.5,1.0), 64,1.0-lifeTime/128);
        Camera cam = ((Game) getParent()).getMainCamera();
        p2.setPos(posX-cam.posX, posY-cam.posY);
        cam.al.addPointLight(p2);
        super.update();
    }
}
