/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.objects;

import com.elthis.objects.component.Component;
import com.mycompany.game.Game;
import java.util.ArrayList;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;

/**
 *
 * @author Elthis
 */
public abstract class GameObject {
    Object parent;
    ArrayList<GameObject> children = new ArrayList();
    ArrayList<Component> components = new ArrayList();
    ArrayList<GameObject> removeList = new ArrayList();
    protected Double posX = 0.0, posY = 0.0, rotation = 0.0;
    public<T extends Object> T getParent()
    {
        return (T)parent;
    }
    public void destroyObject(GameObject go)
    {
        removeList.add(go);
    }
    public Game getGame()
    {
        if(parent instanceof Game)
            return (Game)parent;
        else return ((GameObject)parent).getGame();
    }
    public void setParent(Object obj)
    {
        parent = obj;
    }
    public<T extends GameObject> T getComponent()
    {
        T type = null;
        for(Component go : components){
            
            if((T)go != null)
            {
                type = (T) go;
                break;
            }
                
        }
        return type;
    }
    public ArrayList getComponents()
    {
        ArrayList<Component> tmp = new ArrayList();
        tmp.addAll(this.components);
        children.forEach((go) -> {
            tmp.addAll(go.getComponents());
        });
        return tmp;
    }
    public void addComponent(Component component)
    {
        component.setParent(this);
        components.add(component);
    }
    public void addChild(GameObject obj)
    {
        obj.setParent(this);
        children.add(obj);
    }
    public ArrayList getChildren()
    {
        return children;
    }
    public Double getRotation()
    {
        return rotation;
    }
    public Point2D getPos()
    {
        return new Point2D(posX,posY);
    }
    public void setPos(double x, double y)
    {
        posX = x;
        posY = y;
    }
    public void setPos(Point2D point)
    {
        posX = point.getX();
        posY = point.getY();
    }
    public void init()
    {
        children.stream().forEach((obj) -> {
            obj.init();
        });
    }
    public void update()
    {
        children.stream().forEach((obj) -> {
            obj.update();
        });
        children.removeAll(removeList);
        removeList.clear();
    }
    public void render(GraphicsContext gc, Point2D camera)
    {
        children.stream().forEach((obj) -> {
            obj.render(gc,camera);
        });
        components.stream().forEach((obj) -> {
            obj.render(gc,camera);
        });
    }
}
