/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.graphics;

import com.elthis.objects.GameObject;
import com.elthis.repository.Repository;
import com.elthis.time.Time;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;

/**
 *
 * @author Elthis
 */
public class AnimatedSprite extends GameObject {
    Image spriteSheet;
    Integer width, height;
    Double time;
    Integer currentFrame;
    Integer maxFrames;
    public AnimatedSprite(String repoKey, Integer width, Integer height)
    {
        this.width = width;
        this.height = height;
        spriteSheet = Repository.findByKey(repoKey);
         
    }
    @Override
    public void init()
    {
        time = 0.0;
        currentFrame = 0;
        
        maxFrames = (int)(spriteSheet.getWidth()/width);
    }
    @Override
    public void update()
    {
        time+=Time.getDelta();
        if(time >= 12){
            time-=12;
            currentFrame++;
            currentFrame%=maxFrames;
        }
    }
    @Override
    public void render(GraphicsContext gc, Point2D camera)
    {   
        gc.save();
        
        gc.translate(this.<GameObject>getParent().getPos().getX()-camera.getX(),this.<GameObject>getParent().getPos().getY()-camera.getY());
        gc.rotate(this.<GameObject>getParent().getRotation());
        gc.translate(-width/2, -height/2);
        gc.drawImage(spriteSheet, width*currentFrame, 0, width, height, 0, 0, width, height);
        gc.restore();
    }
    
    public void renderShadow(GraphicsContext gc)
    {
        
    }
    

}
