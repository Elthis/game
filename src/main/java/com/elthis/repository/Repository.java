/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elthis.repository;

import java.util.HashMap;

/**
 *
 * @author Elthis
 */
public abstract class Repository {
    static HashMap<String,Object> map = new HashMap();
    
    static public void add(String key, Object obj)
    {
        map.put(key,obj);
    }
    static  public <T extends Object> T findByKey(String key)
    {
        return (T)map.get(key);
    }
    
    static public void clear()
    {
        map.clear();
    }
}
